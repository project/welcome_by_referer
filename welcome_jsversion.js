/*
	parseUri 1.2.1
	(c) 2007 Steven Levithan <stevenlevithan.com>
	MIT License
*/

function parseUri (str) {
	var	o   = parseUri.options,
		m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
		uri = {},
		i   = 14;
	while (i--) uri[o.key[i]] = m[i] || "";
		uri[o.q.name] = {};
		uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
		if ($1) uri[o.q.name][$1] = $2;
	});
	return uri;
};

parseUri.options = {
	strictMode: false,
	key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
	q:   {
		name:   "queryKey",
		parser: /(?:^|&)([^&=]*)=?([^&]*)/g
	},
	parser: {
		strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
		loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
	}
};

function showWelcome(message) {
	$('#welcome_by_referer_js').html($('#welcome_by_referer_js').html()+message);
}


$(document).ready(function () {
	var settings = Drupal.settings.welcome_by_referer_settings;
	var ref_url = parseUri(document.referrer);
	ref_url['url'] = ref_url['source'];
	ref_url['scheme'] =	ref_url['protocol'];
	ref_url['fragment'] =	ref_url['anchor'];
	var current_url = parseUri(document.URL); 
	current_url['url'] = document.URL;
	if(ref_url['host']!='' && current_url['host']!=ref_url['host']){
		for (key in settings){
			var component = ref_url[settings[key]['url_component']].toLowerCase();
			switch(settings[key]['match_type']) {
				case 'begins with':
					var index = component.indexOf(settings[key]['pattern'].toLowerCase());
					if(index==0) { showWelcome(settings[key]['message']); }
					break;
				case 'ends with':
					if(settings[key]['url_component']=='url' && settings[key]['pattern'].substring(settings[key]['pattern'].length-1,settings[key]['pattern'].length) != '/') {
						settings[key]['pattern'] = settings[key]['pattern'] + '/';
					}
					if (component.substring(component.length-settings[key]['pattern'].length, component.length)==settings[key]['pattern']) {
						showWelcome(settings[key]['message']); 
					}
					break;
				case 'is':
					if(settings[key]['url_component']=='url' && settings[key]['pattern'].substring(settings[key]['pattern'].length-1,settings[key]['pattern'].length) != '/') {
						settings[key]['pattern'] = settings[key]['pattern'] + '/';
					}
					if (component==settings[key]['pattern']) {
						showWelcome(settings[key]['message']); 
					}
					break;								
				case 'contains':
					if(settings[key]['url_component']=='url' && settings[key]['pattern'].substring(settings[key]['pattern'].length-1,settings[key]['pattern'].length) != '/') {
						settings[key]['pattern'] = settings[key]['pattern'] + '/';
					}
					var index = component.indexOf(settings[key]['pattern'].toLowerCase());
					if(index>=0) { showWelcome(settings[key]['message']); }							
						break;
				}
		}
	}	
	
});